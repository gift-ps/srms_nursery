<?php 
    require_once('../scripts/staff_validation.php');
    require_once('../../../config/admin_server.php');
    $add_side_bar = true;
    include_once('../layouts/head_to_wrapper.php');
    include_once('../layouts/topbar.php');

?>

<style>
    .table-width {
    padding-right: 75px;
    padding-left: 75px;
    margin-right: auto;
    margin-left: auto;
    }
    @media (min-width: 768px) {
    .table-width {
        width: 750px;
    }
    }
    @media (min-width: 992px) {
    .table-width {
        width: 970px;
    }
    }
    @media (min-width: 1200px) {
    .table-width {
        width: 1170px;
    }
    }
</style>

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-7">
                <div class="card shadow-s border-0 rounded-lg mt-1">

                    <div class="card-header"><h5 class="text-center my-2">Add new staff</h5></div>
                    <div class="card-body">
                        <form action="#" method="post" onsubmit="return staff_validation();" enctype="multipart/form-data">

                            <table class="table" id="dataTable" width="100%" cellspacing="9">

                                <input id="id"type="hidden" name="id" placeholder="Enter Id">
                                <tr>
                                    <td>Name:</td>
                                    <td class="text-right"><input id="name" type="text" name="name" placeholder="Name" required></td>
                                </tr>
                                <tr>
                                    <td>User name:</td>
                                    <td class="text-right"><input id="name" type="text" name="username" placeholder="Use name" required></td>
                                </tr>
                                <tr>
                                    <td>Password:</td>
                                    <td class="text-right"><input id="password" type="password" name="password" placeholder="Enter Password"></td>
                                </tr>
                                <tr>
                                    <td>Phone Number:</td>
                                    <td class="text-right"><input type="number" name="phone" placeholder="Phone Number" required></td>
                                </tr>
                                <tr>
                                    <td>Email:</td>
                                    <td class="text-right"><input id="email"type="email" name="email" placeholder="Email" required></td>
                                </tr>
                                <tr>
                                    <td>Date of Birth:</td>
                                    <td class="text-right">
                                    <input type="text" name="dob" id="date1" alt="date" class="IP_calendar" title="Y-m-d" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Gender:</td>
                                    <td class="text-right"><input type="radio" name="gender" id="m" value="Male" onclick="teaGender = this.value;"> <label for="m">Male</label> <input type="radio" name="gender" id="f" value="Female" onclick="this.value"> <label for="f"> Female</label></td>
                                </tr>
                                <tr>
                                    <td>Date Hired:</td>
                                    <td class="text-right">
                                        <input type="text" name="hiredate" id="date2" alt="date" class="IP_calendar" title="Y-m-d" readonly>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Address:</td>
                                    <td class="text-right"><input id="address" type="text" name="address" placeholder="Enter Address" ></td>
                                </tr>
                                <tr>
                                    <td>User type:</td>
                                    <td class="text-right">
                                        <div class="form-">
                                        <select name="user_role" class="form-" id="user_role" required>
                                            <option value="lecturer">Teacher</option>
                                            <option value="librarian">Librarian</option>
                                            <option value="manager">Manager</option>
                                            <option value="accountant">Accountant</option>
                                            <option value="admin">Admin</option>
                                            <option value="other">Other</option>
                                        </select>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Salary:</td>
                                    <td class="text-right"><input id="salary" type="text" name="salary" placeholder="eg. 21000" ></td>
                                </tr>
                                <tr>
                                    <td>Picture:</td>
                                    <td class="text-right"><input id="file"type="file" name="file"></td>
                                </tr>
                                <tr>
                                    <td></td>
                                    <td class="text-left"><input class="btn btn-sm btn-primary " type="submit" name="add_staff"value="Submit"></td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php require_once('../layouts/footer_to_end.php'); ?>

