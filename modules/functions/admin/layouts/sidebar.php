    <!-- @Overide some fa styling -->
    <style>
      .navbar-nav i.fas{
        color: #f8f9fc !important;
        font-size: 190% !important;
      }
      .collapse-inner a {
        color: white! important;
      }
      .collapse-inner a:hover{
        background-color: lightgreen !important;
        color: black !important;
      }
      li span{
        color: #f8f9fc;
        font-style: bold!important;
        font-size: 110% !important;
      }
      .sidebar-light .sidebar-brand {
          color: #f8f9fc;
      }
      .sidebar_new_bg{
        background: #0067a9;
      }

    </style>
    
    <ul class="navbar-nav sidebar_new_bg sidebar sidebar-light accordion" id="accordionSidebar" >

          <!-- Sidebar - Brand -->
          <a class="sidebar-brand d-flex align-items-center justify-content-center" href="../index.php">
            <div class="sidebar-brand-icon rotate-n-15">
              <!-- <i class="fas fa-laugh-wink"></i> -->
            </div>
            <?php //Get Title..
                  $query =  "SELECT name FROM school_info ";
                  $results=mysqli_query($db,$query);
                  $row=mysqli_fetch_array($results);
                  $admin_acc_title = $row['name'];
            ?>
            <div class="sidebar-brand-text mx-3"><?php echo $admin_acc_title ?> </div>
          </a>

          <!-- Divider -->
          <hr class="sidebar-divider my-0">

          <!-- Nav Item - Dashboard -->
          <li class="nav-item">
            <a class="nav-link" href="../index.php">
              <i class="fas fa-fw fa-tachometer-alt "></i>
              <span>Dashboard</span></a>
          </li>

          <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#students"
                  aria-expanded="true" aria-controls="students">
                  <i class="fas fa-fw fa-users"></i>
                  <span>Manage Students</span>
              </a>
              <div id="students" class="collapse" aria-labelledby="students" data-parent="#accordionSidebar">
                  <div class="bg-dark py-2 collapse-inner rounded">
                      <a class="collapse-item" href="../students/add_student.php">Add student</a>
                      <a class="collapse-item" href="../students/all_students.php">View all students</a>
                    </div>
              </div>
          </li>

          <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#staff"
                  aria-expanded="true" aria-controls="staff">
                  <i class="fas fa-fw fa-chalkboard-teacher"></i>
                  <span>Manage Staff</span>
              </a>
              <div id="staff" class="collapse" aria-labelledby="staff" data-parent="#accordionSidebar">
                  <div class="bg-dark py-2 collapse-inner rounded">
                      <a class="collapse-item" href="../staff/add_staff.php">Add Staff</a>
                      <a class="collapse-item" href="../staff/all_staff.php">View all staff members</a>

                  </div>
              </div>
          </li>

          <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#hostel"
                  aria-expanded="true" aria-controls="hostel">
                  <i class="fas fa-fw fa-hotel"></i>
                  <span>Manage Hostels</span>
              </a>
              <div id="hostel" class="collapse" aria-labelledby="hostel" data-parent="#accordionSidebar">
                  <div class="bg-dark py-2 collapse-inner rounded">
                      <a class="collapse-item" href="../hostels/add_hostel.php">Add new hostel</a>
                      <a class="collapse-item" href="../hostels/all_hostels.php">View all hostels</a>
                  </div>
              </div>
          </li>

          <li class="nav-item">
              <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#parents"
                  aria-expanded="true" aria-controls="parents">
                  <i class="fas fa-fw fa-house-user"></i>
                  <span>Manage parents</span>
              </a>
              <div id="parents" class="collapse" aria-labelledby="parents" data-parent="#accordionSidebar">
                  <div class="bg-dark py-2 collapse-inner rounded">
                      <a class="collapse-item" href="../parents/add_parent.php">Add parent</a>
                      <a class="collapse-item" href="../parents/all_parents.php">View all parents</a>
                  </div>
              </div>
          </li>


          <li class="nav-item text-color-dark">
            <a class="nav-link" href="../subjects/index.php">
              <i class="fas fa-fw fa fa-book"></i>
              <span>Subjects</span></a>
          </li>

          <li class="nav-item text-color-dark">
            <a class="nav-link" href="../classes/index.php">
              <i class="fas fa-fw fa fa-book-reader"></i>
              <span>Class</span></a>
          </li>
          
          <li class="nav-item text-color-dark">
            <a class="nav-link" href="../school_info/index.php">
              <i class="fas fa-fw fa-school "></i>
              <span>School Infomation</span></a>
          </li>

          <li class="nav-item text-color-dark">
            <a class="nav-link" href="../settings/index.php">
              <i class="fas fa-fw fa-setting "></i>
              <span>Settings</span></a>
          </li>

          <li class="nav-item text-color-dark">
            <a class="nav-link" href="../sms/index.php">
              <i class="fas fa-fw fa-setting "></i>
              <span>SMS Manager</span></a>
          </li>

          <li class="nav-item text-color-dark">
            <a class="nav-link" href="../license/index.php">
              <i class="fas fa-fw fa fa-settings"></i>
              <span>License </span></a>
          </li>

          <!-- Divider -->
          <hr class="sidebar-divider d-none d-md-block">

          <!-- Sidebar Toggler (Sidebar) -->
          <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
          </div>

    </ul>
