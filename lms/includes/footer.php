<!-- footer starts here -->

<footer class="page-footer blue" style="
    position: relative !important;
    margin-top: 350px;
    bottom: 0 !important;">

    <div class="footer-copyright">

      <div class="container row white-text">

        <div class="col s12 m12 center">Powered by <?php echo $app_name.".  &copy; ".date('Y') ?>. </div>

      </div>

    </div>

</footer>

  <!-- footer ends here -->

